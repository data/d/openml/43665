# OpenML dataset: Indian-Liver-Patient-Records

https://www.openml.org/d/43665

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Patients with Liver disease have been continuously increasing because of excessive consumption of alcohol, inhale of harmful gases, intake of contaminated food, pickles and drugs. This dataset was used to evaluate prediction algorithms in an effort to  reduce burden on doctors. 
Content
This data set contains 416 liver patient records and 167 non liver patient records collected from North East of Andhra Pradesh, India.  The "Dataset" column is a class label used to divide groups into liver patient (liver disease) or not (no disease). This data set contains 441 male patient records and 142 female patient records. 
Any patient whose age exceeded 89 is listed as being of age "90".
Columns:

Age of the patient 
Gender of the patient 
Total Bilirubin 
Direct Bilirubin 
Alkaline Phosphotase 
Alamine Aminotransferase 
Aspartate Aminotransferase 
Total Protiens 
Albumin 
Albumin and Globulin Ratio 
Dataset: field used to split the data into two sets (patient with liver disease, or no disease)

Acknowledgements
This dataset was downloaded from the UCI ML Repository:
Lichman, M. (2013). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science.
Inspiration
Use these patient records to determine which patients have liver disease and which ones do not.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43665) of an [OpenML dataset](https://www.openml.org/d/43665). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43665/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43665/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43665/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

